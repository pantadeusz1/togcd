
BUILDDIR=build
THIRDPARTYDIR=thirdparty


ARCH=$(shell uname -i)
CFLAGS_OCV=$(shell pkg-config --cflags opencv)
LIBS_OCV=$(shell pkg-config --libs opencv)

CFLAGS_OCV4=$(shell pkg-config --cflags opencv4)
LIBS_OCV4=$(shell pkg-config --libs opencv4)


INCLUDE=-Wno-psabi -Wall -I${THIRDPARTYDIR}/distance_t -I${THIRDPARTYDIR}/json
CFLAGS= -std=c++17 -DARCH_${ARCH} -fopenmp -O3 ${INCLUDE}
# ${CFLAGS_OCV} ${CFLAGS_OCV4}
LIBS=-fopenmp
# ${LIBS_OCV} ${LIBS_OCV4}

ARCH=$(shell uname -i)

OBJFILES= build/togcd.o build/lodepng.o build/shape.o

all: build build/togcd

build/togcd: ${OBJFILES}
	g++ $^ -o $@ $(LIBS) 

build/%.o : src/%.cpp
	g++ ${CFLAGS} -c $< -o $@

build:
	mkdir -p build

test: all
	build/togcd 5mm_poziom_A_nakretka.png

clean:
	rm -rf build/togcd
	rm -rf build/*.o
	
	
distclean:
	rm -rf build
#	rm -f *.png
#	rm -f result.txt

