/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

#include "shape.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <unordered_set>

#include "lodepng.h"

namespace tp {
namespace shape {

image_t new_image(int width, int height, unsigned char c)
{
    return std::make_tuple(width, height, std::vector<short int>(width * height, c));
}
unsigned save_image(const image_t& img, const std::string& fname)
{
    auto [width, height, data] = img;
    std::vector<unsigned int> tosave;
    std::vector<unsigned char> image(data.size() * 4);
    for (unsigned i = 0; i < data.size(); i++) {
        for (unsigned n = 0; n < 3; n++)
            image[i * 4 + n] = data[i];
        image[i * 4 + 3] = 255;
    }
    return lodepng::encode(fname, image, width, height);
}
image_t load_image(const std::string& fname)
{
    image_t img;
    auto& [width, height, data] = img;
    std::vector<unsigned char> out;
    unsigned error = lodepng::decode(out, width, height, fname);

    // if there's an error, display it
    if (error)
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

    data.resize(width * height);
    for (size_t i = 0; i < width * height; i++)
        data[i] = out[i * 4];
    return img;
}
void draw_circle(image_t& img,
                 const int x0,
                 const int y0,
                 const double r0,
                 const unsigned char color)
{
    double r2 = r0 * r0;
    for (int y = std::max(0.0, y0 - r0 - 1); y < std::min((int)std::get<1>(img), (int)(y0 + r0 + 1)); y++) {
        for (int x = std::max(0.0, x0 - r0 - 1); x < std::min((int)std::get<0>(img), (int)(x0 + r0 + 1)); x++) {
            double l = (x - x0) * (x - x0) + (y - y0) * (y - y0);
            if (l <= r2)
                std::get<2>(img)[(int)(y * std::get<0>(img)) + (int)(x)] = color;
        }
    }
}

image_t draw_shape(const shape_t shape, const double radius, double depth, const int margin)
{
    int width = 0, height = 0;
    generic_position_t<double, 4> transform;
    // generic_position_t<double, 4> multiply = { 1.0, 1.0, 1.0, 1.0 };

    for (const auto& p : shape) {
        width = std::max(width, (int)(p[0]));
        height = std::max(height, (int)(p[0]));
    }

    image_t ret = new_image(width + margin * 2, height + margin * 2, 255);
    for (const auto& p : shape) {
        if (p[2] < 0)
            draw_circle(ret, (int)(p[0]) + margin, (int)(p[0]) + margin, radius, 255 * ((depth - p[2]) / depth));
    }

    return ret;
}


struct hash_f_t {
    inline std::size_t operator()(const std::pair<int, int>& v) const
    {
        return (v.first << 12) + v.second;
    }
};

static auto set_pixel = [](auto& image, int x, int y, const auto c) {
    auto& [width, height, imgdata] = image;
    x = ((unsigned long)x) % width;
    y = ((unsigned long)y) % height;
    imgdata[y * width + x] = c;
};
static auto get_pixel = [](const auto& image, int x, int y) -> unsigned char {
    auto& [width, height, imgdata] = image;
    x = ((unsigned long)x) % width;
    y = ((unsigned long)y) % height;
    return imgdata[y * width + x];
};

auto get_max_around = [](auto& image, auto x0, auto y0) {
    using namespace std;
    auto& [width, height, image_data] = image;
    short max_found = 0;
    for (int x = x0 - 1; x < ((int)x0 + 2); x++) {
        for (int y = y0 - 1; y < ((int)y0 + 2); y++) {
            max_found = std::max(max_found, image_data.at(((y + height) % height) * width + ((x + width) % width)));
        }
    }
    return max_found;
};
auto get_min_around = [](auto& image, auto x0, auto y0) {
    using namespace std;
    auto& [width, height, image_data] = image;
    short min_found = 255;
    for (int x = x0 - 1; x < ((int)x0 + 2); x++) {
        for (int y = y0 - 1; y < ((int)y0 + 2); y++) {
            min_found = std::min(min_found, image_data.at(((y + height) % height) * width + ((x + width) % width)));
        }
    }
    return min_found;
};

unsigned char max_on_image(const image_t& image0)
{
    auto [width, height, image_data] = image0;
    return *std::max_element(image_data.begin(), image_data.end());
}

namespace image_processing_to_gcode_complete {
enum direction_e { L,
                   LU,
                   U,
                   UR,
                   R,
                   RD,
                   D,
                   DL,
                   Z
                 };
const std::map<direction_e, std::pair<int, int>> directions = {
    {L, {-1, 0}},
    {LU, {-1, -1}},
    {U, {0, -1}},
    {UR, {1, -1}},
    {R, {1, 0}},
    {RD, {1, 1}},
    {D, {0, 1}},
    {DL, {-1, 1}},
    {Z, {0, 0}}
};
class agent_t
{
public:
    int x, y;
    direction_e direction;
    std::function<bool(short int, short int)> cond;
    direction_e touching(image_t& image, int xv, int yv)
    {
        int i = 0;
        for (auto& [k, v] : directions) {
            if (((i % 2) == 0) && (cond(get_pixel(image, xv, yv), get_pixel(image, xv + v.first, yv + v.second))))
                return k;
            i++;
        }
        return Z;
    }
    static direction_e touching(image_t& image, int xv, int yv, std::function<bool(short int currentColor, short int touchedColor)> cond)
    {
        int i = 0;
        for (auto& [k, v] : directions) {
            if (((i % 2) == 0) && (cond(get_pixel(image, xv, yv), get_pixel(image, xv + v.first, yv + v.second))))
                return k;
            i++;
        }
        return Z;
    }
    void step(image_t& image)
    {
        // auto& [width, height, image_data] = image;
        for (int i = 0; i < 8; i++) {
            auto ndir = static_cast<direction_e>(((int)direction - i + 3 + 8) % 8);
            auto nx = x + directions.at(ndir).first;
            auto ny = y + directions.at(ndir).second;
            if (touching(image, nx, ny) != Z) {
                if ((nx >= 0) && (ny >= 0) && (nx < (int)std::get<0>(image)) && (ny < (int)std::get<1>(image))) {
                    direction = static_cast<direction_e>(((int)ndir + 8) % 8);
                    x = nx;
                    y = ny;
                    return;
                }
            }
        }
    }
};
// create shape that is on 0 level and touches any value around
auto get_shape = [](auto& image, int x0, int y0, auto condition) {
    auto& [width, height, image_data] = image;
    shape_t shape;
    int x = x0, y = y0;
    agent_t agent = {x, y, U, condition};
    agent.direction = static_cast<direction_e>(((int)agent.touching(image, agent.x, agent.y) + 8 - 3) % 8);
    if (agent.direction == Z)
        throw std::invalid_argument("the point is not on the edge");
    agent.step(image);
    while (true) {
        agent.step(image);
        raspigcd::generic_position_t<double, 3> next_pos = {(double)agent.x, (double)agent.y, ((double)image_data[y * width + x] - 255.0)};
        if (shape.size() > 1) {
            if ((shape.back() == shape.front()) && (shape[1] == next_pos))
                return shape;
        }
        shape.push_back(next_pos);
    }
    return shape;
};

auto make_shape_order_correct = [](auto& orig_dilated_image, auto shape) {
    auto& [width, height, orig_dilated_image_data] = orig_dilated_image;

    if (shape.size() < 2)
        return shape;

    auto v = shape[1] - shape[0];
    std::swap(v[0], v[1]);
    v[1] *= -1.0;
    auto ptchk = shape[0] + v;
    auto ptchk2 = shape[0] - v;
    if ((orig_dilated_image_data[(int)(shape[0][1] * width + shape[0][0])] >= orig_dilated_image_data[(int)(ptchk[1] * width + ptchk[0])]) || (orig_dilated_image_data[(int)(shape[0][1] * width + shape[0][0])] <= orig_dilated_image_data[(int)(ptchk2[1] * width + ptchk2[0])])) {
        std::reverse(shape.begin(), shape.end());
    }
    return shape;
};
auto make_circle_points = [](auto tool_r) {
    std::vector<std::pair<int, int>> pts;
    auto r = tool_r;
    for (int x = -r - 1; x < r + 1; x++)
        for (int y = -r - 1; y < r + 1; y++)
            if (sqrt(x * x + y * y) <= r)
                pts.push_back({x, y});
    return pts;
};
auto draw_path_circle = [](auto& image, auto& image_consumed, auto& p, auto& circle_points) {
    auto& [width, height, imagedata] = image;
    auto& [ic_width, ic_height, ic_image_data] = image_consumed;
    std::get<2>(image).at(p[1] * width + p[0]) = 255;
    for (auto& cp : circle_points) {
        int x = p[0] + cp.first;
        int y = p[1] + cp.second;
        // we put 255 only if the z value of the path is lower than the current pixel
        if ((x >= 0) && (x < (int)width) && (y >= 0) && (y < (int)height) && (std::get<2>(image)[y * width + x] <= std::get<2>(image)[p[1] * width + p[0]])) {
            std::get<2>(image)[y * width + x] = 255;
            ic_image_data[y * width + x] = 0;
        }
    }
};
auto draw_path_using_circle = [](auto& image, auto& image_consumed, auto& shape, auto& circle_points) {
    std::map<std::pair<int, int>, std::vector<std::pair<int, int>>> directions = {
        {{-1, 0}, {}},
        {{-1, -1}, {}},
        {{0, -1}, {}},
        {{1, -1}, {}},
        {{1, 0}, {}},
        {{1, 1}, {}},
        {{0, 1}, {}},
        {{-1, 1}, {}}
    };
    for (auto& cpt : circle_points) {
        if (cpt.first == 0) {
            directions[ {-1, 0}].push_back(cpt);
            directions[ {1, 0}].push_back(cpt);
        }
        if (cpt.first > 0) { // ->
            directions[ {1, 0}].push_back(cpt);
            directions[ {1, -1}].push_back(cpt);
            directions[ {1, 1}].push_back(cpt);
        }
        if (cpt.first < 0) { // ->
            directions[ {-1, 0}].push_back(cpt);
            directions[ {-1, -1}].push_back(cpt);
            directions[ {-1, 1}].push_back(cpt);
        }
        if (cpt.second == 0) {
            directions[ {0, -1}].push_back(cpt);
            directions[ {0, 1}].push_back(cpt);
        }
        if (cpt.second > 0) { // ->
            directions[ {0, 1}].push_back(cpt);
            directions[ {-1, 1}].push_back(cpt);
            directions[ {1, 1}].push_back(cpt);
        }
        if (cpt.second < 0) { // ->
            directions[ {0, -1}].push_back(cpt);
            directions[ {-1, -1}].push_back(cpt);
            directions[ {1, -1}].push_back(cpt);
        }
        directions[ {0, 0}].push_back(cpt);
    }
    raspigcd::generic_position_t<double, 3> pprev;
    if (shape.size() > 0) {
        pprev = shape.front();
    }
    for (auto& p : shape) {
        auto dp = p - pprev;
        draw_path_circle(image, image_consumed, p, directions[ {(int)dp[0], (int)dp[1]}]); // circle_points);
        pprev = p;
    }
};

} // namespace image_processing_to_gcode_complete

/**
 * @brief
 *
 * @param image0
 * @param tool_r
 * @param do_the_areas the radius of "tool" for the area cutting
 * @return std::list<shape_t>
 */
std::list<shape_t> image_to_shapes_full(const image_t& image0, double tool_r, double do_the_areas_r)
{
    using namespace std;
    using namespace image_processing_to_gcode_complete;
    std::list<shape_t> shapes = {};
    static const std::vector<std::array<int, 2>> directions_to_check = {
        {-1, 0},
        {-1, -1},
        {0, -1},
        {1, -1},
        {1, 0},
        {1, 1},
        {0, 1},
        {-1, 1}
    };
    static const std::vector<std::array<int, 2>> directions_to_move = {
        {-1, 0},
        {0, -1},
        {1, 0},
        {0, 1},
    };
    std::cerr << "do dilate ..." << std::endl;
    auto image = [&]() {
        if (tool_r > 0.5) return image_dilate(image0, tool_r);
        else return image0;
    }
    (); // first stage - fix tool size
    const auto image_dilated_zero = image;
    save_image(image, "debug.png");
    std::cerr << "do dilate ... OK" << std::endl;
    auto& [width, height, image_data] = image;
    auto orig_dilated_image = image;

    if ((width == 0) || (height == 0))
        return {};
    image_t image_consumed = new_image(width, height, 255); // marks paths already done
    auto& [ic_width, ic_height, ic_image_data] = image_consumed;

    // prepare circle
    auto circle_points = make_circle_points(tool_r);


    // outer contours (touching 0)
    for (unsigned int y = 1; y < height - 1; y++) {
        for (unsigned int x = 1; x < width - 1; x++) {
            if ((agent_t::touching(image, x, y, [](auto currentColor, auto secondColor) {
            return (currentColor == 0) && (secondColor > 0);
            }) != Z) && (ic_image_data.at(y * width + x) == 255)) {
                auto new_shape = make_shape_order_correct(orig_dilated_image, get_shape(image, x, y, [](auto a, auto b) {
                    return (a == 0) && (b > 0);
                }));
                shapes.push_back(new_shape);
                // draw_path_using_circle(image, image_consumed, shapes.back(), circle_points);
                for (auto& p : new_shape)                      // shapes.back())
                    ic_image_data.at(p[1] * width + p[0]) = 0; // p[2] + 255;
                // draw_path_using_circle(image, image_consumed, new_shape, circle_points);
                std::cerr << "shape (" << new_shape.size() << ") ...  OK" << std::endl;
            }
        }
    }

    std::cerr << "outer contours done. " << shapes.size() << std::endl;
    auto shapes_outer = shapes;
    shapes.clear();
    // if we would like to make area mode
    if (do_the_areas_r > 0.01) {
        std::cerr << "doing inner contours. " << shapes.size() << std::endl;
        circle_points = make_circle_points(do_the_areas_r);
        if (circle_points.size() < 4)
            circle_points = make_circle_points(2.0);
        tool_r = do_the_areas_r;

        auto new_shapes = shapes;
        if (get_pixel(image, 0, 0) == 0)
            image = flood_fill(0, 0, image, 255); // make sure we don't repeat outermost edges
        else
            std::cerr << "Warning - the edge is object - the value on 0,0 is not 0" << std::endl;
        // for every shape we must draw path using circle and put 255 on it.
        for (auto& shape : new_shapes) {
            // std::cerr << "shape ..." << std::endl;
            draw_path_using_circle(image, image_consumed, shape, circle_points);
            // std::cerr << "shape ...  OK" << std::endl;
        }

        new_shapes.clear();

        auto append_shape = [&](shape_t& new_shape) {
            if (new_shape.size() == 0)
                return;
            if (shapes.size() > 0) {
                auto min_elem = std::min_element(std::begin(shapes), std::end(shapes), [&new_shape](const auto& l, const auto& r) {
                    return ((l.front() - new_shape.back()).length() < (r.front() - new_shape.back()).length());
                });

                if ((min_elem->front() - new_shape.back()).length() < tool_r * 3.0) {
                    // min_elem->splice(min_elem->end(), new_shape);
                    min_elem->insert(min_elem->begin(), new_shape.begin(), new_shape.end());
                } else {
                    shapes.push_back(new_shape);
                }
            } else {
                shapes.push_back(new_shape);
            }
            new_shapes.push_back(new_shape);
        };

        for (unsigned int y = 1; y < height - 1; y++) {
            for (unsigned int x = 1; x < width - 1; x++) {
                // now find point that is on path touching 255 pixel value
                if ((agent_t::touching(image, x, y, [](auto a, auto b) {
                return (a < 255) && (b == 255);
                }) != Z) && (ic_image_data.at(y * width + x) == 255)) {
                    auto new_shape = get_shape(image, x, y, [](auto a, auto b) {
                        return (a < 255) && (b == 255);
                    });
                    new_shape = make_shape_order_correct(orig_dilated_image, new_shape);
                    append_shape(new_shape);
                    // we must go back on y to find another path that potentially can touch current path
                    if (y > (tool_r + 1)) {
                        y = y - (tool_r + 1);
                    } else {
                        y = 1;
                    }
                    // std::cerr << "shape at " << x << " " << y << " number " << shapes.size() << std::endl;
                    // draw using shape
                    draw_path_using_circle(image, image_consumed, new_shape, circle_points);
                    // std::cerr << "shape at " << x << " " << y << " number " << shapes.size() << "  ...  OK" << std::endl;
                }
            }
        }


        std::list<shape_t> filtered_inner_shapes = {};
        for (const auto& shape : shapes) {
            shape_t filtered_shape;
            for (const auto& p : shape) {
                if (p[2] > -255) filtered_shape.push_back(p);
            }
            if (filtered_shape.size() > 0) {
                filtered_inner_shapes.push_back(filtered_shape);
            }
        }
        shapes = filtered_inner_shapes;
    } else {
        /* auto ne_outer_shapes = shapes_outer;
        shapes_outer.clear();
        for (auto &shape : shapes_outer ) {
            bool append_it = true;
            for (auto &p : shape) if (p.at(2) > -255 ) {
                std::cerr << p << std::endl;
                append_it = false; break;
            }
            if (append_it) shapes_outer.push_back(shape);
        } */
    }

    // remove shapes that goes on the 0 z
    std::reverse(shapes_outer.begin(), shapes_outer.end());
    shapes.insert(shapes.end(), shapes_outer.begin(), shapes_outer.end());
    std::cerr << "contours generated. " << shapes.size() << std::endl;
    for (auto &s : shapes) {
        for (auto &p: s) {
            p[2] = std::get<2>(orig_dilated_image)[(int)(p[1]*width+p[0])];
        }
    }
    return shapes;
}


image_t image_dilate(const image_t image0, const double r)
{
    double d = 2.0 * r;

    auto r_0 = d / 2.0;
    auto r2 = r_0 * r_0;
    std::vector<std::pair<int, int>> ballPositions;
    ballPositions.reserve(r2 * 4);
    for (double x = -r_0 - 1; x <= r_0 + 1; x++) {
        for (double y = -r_0 - 1; y <= r_0 + 1; y++) {
            auto d = x * x + y * y;
            if (d <= r2) {
                ballPositions.push_back({x - 1, y});
            }
        }
    }
    const auto& [width, height, image0data] = image0;
    image_t marked{width, height, std::vector<short int>(image0data.size())};
    auto ret = image0;

    std::vector<std::pair<int, int>> directionsToCheckEdge = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

    #pragma omp parallel for
    for (int y = 0; y < (int)height; y++) {
        for (int x = 0; x < (int)width; x++) {
            unsigned char a = get_pixel(image0, x, y);
            for (const auto& dir : directionsToCheckEdge) {
                if (a > get_pixel(image0, (x + dir.first), (y + dir.second))) {
                    set_pixel(marked, x + 1, y, a);
                }
            }
        }
    }

    for (int y = 0; y < (int)height; y++) {
        for (int x = 0; x < (int)width; x++) {
            // edge is marked, time to do the dilate operation on it
            auto a = get_pixel(marked, x, y);
            if (a > 0) {
                for (const auto& p : ballPositions)
                    if ((x + p.first >= 0) && ((x + p.first) < (int)width)
                            &&(y + p.second >= 0) && ((y + p.second) < (int)height))
                    {
                        auto b = get_pixel(ret, x + p.first, y + p.second);
                        auto c = (a > b) ? a : b;
                        set_pixel(ret, x + p.first, y + p.second, c);
                    }
            }
        }
    }
    return ret;
}

image_t flood_fill(int x0, int y0, image_t image, unsigned char c)
{
    using namespace std;
    unordered_set<pair<int, int>, hash_f_t> edge_pts = {{x0, y0}};
    unordered_set<pair<int, int>, hash_f_t> edge_pts_new;
    auto v0 = get<2>(image).at(y0 * get<0>(image) + x0);
    if ((int)c == (int)v0)
        return image;
    while (edge_pts.size() > 0) {
        for (auto e : edge_pts) {
            auto& [x, y] = e;
            x = (x + get<0>(image)) % get<0>(image);
            y = (y + get<1>(image)) % get<1>(image);
            get<2>(image).at(y * get<0>(image) + x) = c;
            if (get<2>(image).at(((y + get<1>(image) + 1) % get<1>(image)) * get<0>(image) + ((x + get<0>(image)) % get<0>(image))) == v0) {
                edge_pts_new.insert({x, y + 1});
            }
            if (get<2>(image).at(((y + get<1>(image)) % get<1>(image)) * get<0>(image) + ((x + get<0>(image) + 1) % get<0>(image))) == v0) {
                edge_pts_new.insert({x + 1, y});
            }
            if (get<2>(image).at(((y + get<1>(image) - 1) % get<1>(image)) * get<0>(image) + ((x + get<0>(image)) % get<0>(image))) == v0) {
                edge_pts_new.insert({x, y - 1});
            }
            if (get<2>(image).at(((y + get<1>(image)) % get<1>(image)) * get<0>(image) + ((x + get<0>(image) - 1) % get<0>(image))) == v0) {
                edge_pts_new.insert({x - 1, y});
            }
        }
        swap(edge_pts, edge_pts_new);
        edge_pts_new.clear();
    }

    return image;
}


} // namespace shape
} // namespace tp
