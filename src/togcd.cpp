/**
 * @file main.cpp
 * @author Tadeusz Puźniakowski
 * @brief Simple PNG to gcode generator with support for tool width
 * @version 0.1
 * @date 2022-02-01
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "shape.hpp"

#include <iostream>
#include <map>
#include <vector>
#include <any>

inline double dpi_to_dpmm(double p, double dpi)
{
    return p * 25.4 / dpi;
}
inline double dpmm_to_dpi(double p, double dpi)
{
    return p * dpi / 25.4;
}

std::map < std::string,std::vector<std::string>> parametersDescriptions() {
    std::map < std::string,std::vector<std::string>> ret;
    // description, unit, default, type(string, int, double), [min],[max]
    ret["dpi"] = {"resolution of the source image","dpi","300","double","10","1200"};
    ret["tool_d"] = {"diameter of the tool","mm","2","double","0","5"};
    ret["tool_d_areas"] = {"the thickness of areas cut or 0 if no areas","mm","2","double","0","5"};
    ret["drill_depth"] = {"how deep do we need to cut the material","mm","3","double","0","30"};
    ret["g1feedrate"] = {"speed of cutting","mm/s","6","double","0.5","120"};
    ret["g1drillfeedrate"] = {"speed of idle movements","mm/s","0.5","double","10","120"};
    ret["multipass"] = {"how many cutting iterations","","1","int","1","100"};
    ret["safe_cut"] = {"how high should be the left over connection that keeps elements in material","","128","int","0","255"};
    return ret;
}

void dumpParameters(std::map<std::string, std::any> parameters,
                    std::map < std::string,std::vector<std::string>> parameters_descritpion) {
    std::cout << "{" << std::endl;
    for (auto [param_name,v] : parametersDescriptions()) {
        std::string valstr;
        auto typen = v.at(3);
        if (typen == "string") {
            valstr = std::any_cast<std::string>(parameters[param_name]) ;
        } else if (typen == "int") {
            valstr = std::to_string(std::any_cast<int>(parameters[param_name]));
        } else if (typen == "double") {
            valstr = std::to_string(std::any_cast<double>(parameters[param_name]));
        }
        std::cout
                << "  \"" << param_name <<"\": {" << std::endl
                << "    \"typename\": \""  << typen << "\"," << std::endl
                << "    \"value\": \"" << valstr << "\"," << std::endl
                << "    \"unit\": \"" << v.at(1)  << "\"," << std::endl
                << "    \"default\": \"" << v.at(2)  << "\"," << std::endl
                << "    \"description\": \"" << v.at(0) << "\"" << std::endl
                << "  }"  << "," << std::endl;

    }
    std::cout << "}" << std::endl;
}

std::string setDefaultsAndParameters(std::map<std::string, std::any> &parameters,
                                     std::map < std::string,std::vector<std::string>> &parameters_descritpion,
                                     std::vector<std::string> arguments
                                    ) {
    std::string input_fname;
    for (auto [param_name,v]: parameters_descritpion) {
        auto value = v.at(2);
        auto typen = v.at(3);
        if (typen == "string") {
            parameters[param_name] = value;
        } else if (typen == "int") {
            parameters[param_name] = std::stoi(value);
        } else if (typen == "double") {
            parameters[param_name] = std::stod(value);
        }
    }

    for (auto e : arguments) {
        if (e == "--help") {
                std::cout << "see --params for full list of parameters" << std::endl;
        } else if (e == "--params") {
                dumpParameters(parameters,parameters_descritpion);
        } else
            try {
                auto eqp = e.find('=');
                if (eqp == std::string::npos) {
                    input_fname = e;
                } else {
                    auto param_name = e.substr(0, eqp);
                    if (parameters.count(param_name)) {
                        if (parameters_descritpion[param_name].at(3) == "string") {
                            parameters[param_name] = e.substr(eqp + 1);
                        } else if (parameters_descritpion[param_name].at(3) == "int") {
                            parameters[param_name] = std::stoi(e.substr(eqp + 1));
                        } else if (parameters_descritpion[param_name].at(3) == "double") {
                            parameters[param_name] = std::stod(e.substr(eqp + 1));
                        }
                    }
                }
            } catch (const std::exception& ex) {
            }
    }
    return input_fname;
}

int main(int argc, char** argv)
{
    std::map<std::string, std::any> parameters;
    std::map < std::string,std::vector<std::string>> parameters_descritpion = parametersDescriptions();
    std::string input_fname = setDefaultsAndParameters(parameters,parameters_descritpion, std::vector<std::string>(argv + 1, argv + argc));


#define dpi (std::any_cast<double>(parameters.at("dpi")))
#define tool_d (std::any_cast<double>(parameters.at("tool_d")))
#define tool_d_areas (std::any_cast<double>(parameters.at("tool_d_areas")))
#define drill_depth (std::any_cast<double>(parameters.at("drill_depth")))
#define g1feedrate (std::any_cast<double>(parameters.at("g1feedrate")))
#define g1drillfeedrate (std::any_cast<double>(parameters.at("g1drillfeedrate")))
#define multipass (std::any_cast<int>(parameters.at("multipass")))
#define safe_cut (std::any_cast<int>(parameters.at("safe_cut")))

    if (input_fname == "") return 0;

    auto image_loaded = tp::shape::load_image(input_fname);

    int image_height = std::get<1>(image_loaded);
    auto color_to_z = [&](u_char c) {
        return ((double)(c - 255) / 255.0) * drill_depth;
    };
    auto mod_x = [&](double x) {
        return dpi_to_dpmm(x, dpi);
    };
    auto mod_y = [&](double y) {
        return dpi_to_dpmm(image_height - y, dpi);
    };
    using namespace tp::shape;
    std::list<shape_t> contours_generated = image_to_shapes_full(image_loaded, dpmm_to_dpi(tool_d, dpi) / 2.0, dpmm_to_dpi(tool_d_areas, dpi) / 2.0); // tool_d);

    if (safe_cut != 0) {
        double r = dpmm_to_dpi(tool_d, dpi);
        for (auto& contour : contours_generated) {
            int maximal_idx = 0;

            for (int i = 0; i < contour.size() ; i++) {
                if (((contour[0] - contour[maximal_idx]).length()) > ((contour[i] - contour[maximal_idx]).length())) {
                    maximal_idx = i;
                }
            }

            for (auto & p : contour) {
                auto pX = p;
                auto a = contour[0];
                auto b = contour[maximal_idx];
                pX[2] = 0;
                a[2] = 0;
                b[2] = 0;
                if (((pX - a).length() <= r) || ((pX - b).length() <= r)) {
                    p[2] = std::max((double)safe_cut,p[2]);
                }
            }
        }
    }

    for (auto& contour : contours_generated) {
        contour = optimize_path_dp(contour, 1.5);
    }
    std::cout << "M3" << "\n";
    for (auto contour : contours_generated) {
        std::cout << "G0Z10" << "\n";
        std::cout << "G0X" << mod_x(contour.at(0)[0]) << "Y" << mod_y(contour.at(0)[1]) << "\n";
        std::cout << "G0Z0.1" << "\n";
        double prev_z = 0.1;

        for (int i = 1; i <= multipass; i++) {
            // std::reverse( contour.begin(), contour.end() );
            for (auto p : contour) {
                double z = (color_to_z(p[2]) * i / multipass);
                std::cout << "G1X" << mod_x(p[0]) << "Y" << mod_y(p[1]);
                std::cout << "\n";
                if (z != prev_z) {
                    std::cout << "G1Z" << z << "F" << g1drillfeedrate << "\n";
                    std::cout << "G1F" << g1feedrate << "\n";
                }
                prev_z = z;
            }
        }
    }
    std::cout << "G0Z10" << "\n";
    std::cout << "G0X0Y0" << "\n";
    std::cout << "G0Z0" << "\n";
    std::cout << "M5" << "\n";

    return 0;
}
